
module cwx.editor.gui.dwt.eventwindow;

import cwx.area;
import cwx.card;
import cwx.event;
import cwx.menu;
import cwx.path;
import cwx.skin;
import cwx.summary;
import cwx.types;
import cwx.utils;

import cwx.editor.gui.dwt.areawindow;
import cwx.editor.gui.dwt.commons;
import cwx.editor.gui.dwt.dmenu;
import cwx.editor.gui.dwt.dprops;
import cwx.editor.gui.dwt.dskin;
import cwx.editor.gui.dwt.dutils;
import cwx.editor.gui.dwt.eventtreeview;
import cwx.editor.gui.dwt.eventview;
import cwx.editor.gui.dwt.undo;

import std.conv;

import org.eclipse.swt.all;

class EventWindow : TopLevelPanel, SashPanel, TCPD {
private:
	int _readOnly = 0;
	Summary _summ;
	EventTreeOwner _eto;
	Commons _comm;
	Props _prop;
	UndoManager _undo;

	Composite _win;

	EventView _eview;

	bool _refUndo = false;
	void refUndoMax() { mixin(S_TRACE);
		if (!_refUndo) return;
		_undo.max = _prop.var.etc.undoMaxEvent;
	}
public:
	this (Commons comm, Props prop, Summary summ, Composite parent, Shell parent2, EventTreeOwner eto, UndoManager undo, bool readOnly) { mixin(S_TRACE);
		Composite contPane;
		_win = new Composite(parent, SWT.NONE);
		contPane = _win;
		_win.setData(new TLPData(this));
		contPane.setLayout(windowGridLayout(1, true));
		_prop = prop;
		_summ = summ;
		_eto = eto;
		_comm = comm;
		_readOnly = readOnly ? SWT.READ_ONLY : SWT.NONE;
		_refUndo = undo is null;
		_undo = undo ? undo : new UndoManager(_prop.var.etc.undoMaxEvent);

		if (_readOnly) { mixin(S_TRACE);
			_comm.closeAdds.add(&closeAdds);
		} else { mixin(S_TRACE);
			if (cast(Area)_eto) {
				_comm.delArea.add(&deleteOwnerArea);
				_comm.refArea.add(&refOwnerArea);
			} else if (cast(Battle)_eto) {
				_comm.delBattle.add(&deleteOwnerBattle);
				_comm.refBattle.add(&refOwnerBattle);
			} else if (cast(Package)_eto) {
				_comm.delPackage.add(&deleteOwnerPackage);
				_comm.refPackage.add(&refOwnerPackage);
			} else if (cast(SkillCard)_eto) {
				_comm.delSkill.add(&deleteOwnerSkillCard);
				_comm.refSkill.add(&refOwnerSkillCard);
			} else if (cast(ItemCard)_eto) {
				_comm.delItem.add(&deleteOwnerItemCard);
				_comm.refItem.add(&refOwnerItemCard);
			} else if (cast(BeastCard)_eto) {
				_comm.delBeast.add(&deleteOwnerBeastCard);
				_comm.refBeast.add(&refOwnerBeastCard);
			} else { mixin(S_TRACE);
				assert (0);
			}
			_comm.replText.add(&refreshTitle);
			_comm.refUndoMax.add(&refUndoMax);
		}
		_win.addDisposeListener(new class DisposeListener {
			override void widgetDisposed(DisposeEvent e) { mixin(S_TRACE);
				if (_readOnly) { mixin(S_TRACE);
					_comm.closeAdds.remove(&closeAdds);
				} else { mixin(S_TRACE);
					if (cast(Area)_eto) {
						_comm.delArea.remove(&deleteOwnerArea);
						_comm.refArea.remove(&refOwnerArea);
					} else if (cast(Battle)_eto) {
						_comm.delBattle.remove(&deleteOwnerBattle);
						_comm.refBattle.remove(&refOwnerBattle);
					} else if (cast(Package)_eto) {
						_comm.delPackage.remove(&deleteOwnerPackage);
						_comm.refPackage.remove(&refOwnerPackage);
					} else if (cast(SkillCard)_eto) {
						_comm.delSkill.remove(&deleteOwnerSkillCard);
						_comm.refSkill.remove(&refOwnerSkillCard);
					} else if (cast(ItemCard)_eto) {
						_comm.delItem.remove(&deleteOwnerItemCard);
						_comm.refItem.remove(&refOwnerItemCard);
					} else if (cast(BeastCard)_eto) {
						_comm.delBeast.remove(&deleteOwnerBeastCard);
						_comm.refBeast.remove(&refOwnerBeastCard);
					} else { mixin(S_TRACE);
						assert (0);
					}
					_comm.replText.remove(&refreshTitle);
					_comm.refUndoMax.remove(&refUndoMax);
				}
			}
		});
		{ mixin(S_TRACE);
			_eview = new typeof(_eview)(comm, prop, summ, null, eto, contPane, _undo, _readOnly != SWT.NONE);
			_eview.setLayoutData(new GridData(GridData.FILL_BOTH));
		}
		appendMenuTCPD(_comm, this, this, true, true, true, true, true);
		if (cast(Area)_eto || cast(Battle)_eto) {
			putMenuAction(MenuID.EditScene, () => openScene(false), null);
			putMenuAction(MenuID.EditSceneDup, () => openScene(true), null);
		}
		putMenuAction(MenuID.EditEventDup, () => openDup(), null);
		putMenuAction(MenuID.Undo, &_eview.undo, () => !_readOnly && _undo.canUndo);
		putMenuAction(MenuID.Redo, &_eview.redo, () => !_readOnly && _undo.canRedo);
		putMenuAction(MenuID.Up, &_eview.up, &_eview.canUp);
		putMenuAction(MenuID.Down, &_eview.down, &_eview.canDown);
		putMenuAction(MenuID.Comment, &_eview.writeComment, &_eview.canWriteComment);
		putMenuAction(MenuID.ToScript, &_eview.toScript, &_eview.canToScript);
		putMenuAction(MenuID.ToScript1Content, &_eview.toScript1Content, &_eview.canToScript);
		putMenuAction(MenuID.ToScriptAll, &_eview.toScriptAll, &_eview.canToScriptAll);
		putMenuAction(MenuID.EventToPackage, &_eview.startToPackage, &_eview.canEventToPackage);
		putMenuAction(MenuID.StartToPackage, &_eview.startToPackage, &_eview.canStartToPackage);
		putMenuAction(MenuID.WrapTree, &_eview.wrapTree, &_eview.canWrapTree);
		putMenuAction(MenuID.SelectConnectedResource, &_eview.eventTreeView.selectConnectedResource, &_eview.eventTreeView.canSelectConnectedResource);
		putMenuAction(MenuID.FindID, &_eview.findStartUsers, &_eview.canFindStartUsers);
		putMenuAction(MenuID.EditProp, &_eview.edit, &_eview.canEdit);
		putMenuAction(MenuID.Cut1Content, &_eview.cut1Content, &_eview.canCut1Content);
		putMenuAction(MenuID.Copy1Content, &_eview.copy1Content, &_eview.canCopy1Content);
		putMenuAction(MenuID.Delete1Content, &_eview.del1Content, &_eview.canDel1Content);
		putMenuAction(MenuID.PasteInsert, &_eview.pasteInsert, &_eview.canPasteInsert);
		putMenuAction(MenuID.SwapToParent, &_eview.swapToParent, &_eview.canSwapToParent);
		putMenuAction(MenuID.SwapToChild, &_eview.swapToChild, &_eview.canSwapToChild);
		putMenuAction(MenuID.SelectCurrentEvent, &_eview.selectCurrentEvent, &_eview.canSelectCurrentEvent);

		auto d = contPane.getDisplay();
		auto tl = new class Listener {
			override void handleEvent(Event e) { mixin(S_TRACE);
				auto tabf = cast(CTabFolder)contPane.getParent();
				if (!tabf) return;
				if (!tabf.getSelection()) return;
				if (contPane is tabf.getSelection().getControl()) { mixin(S_TRACE);
					_eview.openToolWindow();
				}
			}
		};
		d.addFilter(SWT.FocusIn, tl);
		.listener(contPane, SWT.Dispose, { mixin(S_TRACE);
			d.removeFilter(SWT.FocusIn, tl);
		});

		_eview.refresh();
		refreshTitle();
	}

	@property
	override
	Composite shell() { mixin(S_TRACE);
		return _win;
	}
	@property
	UndoManager undoManager() { mixin(S_TRACE);
		return _undo;
	}
	private void openScene(bool canDuplicate) { mixin(S_TRACE);
		if (auto area = cast(Area)_eto) { mixin(S_TRACE);
			auto duplicateBase = canDuplicate ? cast(AreaSceneWindow)_comm.areaWindowFrom(area.cwxPath(true), false) : null;
			_comm.openAreaScene(_prop, _summ, area, true, duplicateBase);
		} else if (auto area = cast(Battle)_eto) { mixin(S_TRACE);
			auto duplicateBase = canDuplicate ? cast(BattleSceneWindow)_comm.areaWindowFrom(area.cwxPath(true), false) : null;
			_comm.openAreaScene(_prop, _summ, area, true, duplicateBase);
		} else assert (0);
	}
	private void openDup() { mixin(S_TRACE);
		_eview.openDup();
	}
	private void deleteOwner(EventTreeOwner a) { mixin(S_TRACE);
		if (_eto is a) { mixin(S_TRACE);
			_comm.close(_win);
		}
	}
	private void deleteOwnerArea(Area a) { deleteOwner(a); }
	private void deleteOwnerBattle(Battle a) { deleteOwner(a); }
	private void deleteOwnerPackage(Package a) { deleteOwner(a); }
	private void deleteOwnerSkillCard(CWXPath owner, SkillCard a) { deleteOwner(a); }
	private void deleteOwnerItemCard(CWXPath owner, ItemCard a) { deleteOwner(a); }
	private void deleteOwnerBeastCard(CWXPath owner, BeastCard a) { deleteOwner(a); }

	private void refOwner(EventTreeOwner a) { mixin(S_TRACE);
		if (_eto is a) { mixin(S_TRACE);
			refreshTitle();
		}
	}
	private void refOwnerArea(Area a) { refOwner(a); }
	private void refOwnerBattle(Battle a) { refOwner(a); }
	private void refOwnerPackage(Package a) { refOwner(a); }
	private void refOwnerSkillCard(SkillCard a) { refOwner(a); }
	private void refOwnerItemCard(ItemCard a) { refOwner(a); }
	private void refOwnerBeastCard(BeastCard a) { refOwner(a); }

	private void closeAdds(Summary summ) { mixin(S_TRACE);
		if (_summ is summ) { mixin(S_TRACE);
			_comm.close(_win);
		}
	}
	@property
	override
	Image image() { mixin(S_TRACE);
		if (cast(Area)_eto) {
			return _prop.images.areaEventTreeView;
		} else if (cast(Battle)_eto) {
			return _prop.images.battleEventTreeView;
		} else if (cast(Package)_eto) {
			return _prop.images.packages;
		} else if (cast(SkillCard)_eto) {
			return _prop.images.skill;
		} else if (cast(ItemCard)_eto) {
			return _prop.images.item;
		} else if (cast(BeastCard)_eto) {
			return _prop.images.beast;
		} else { mixin(S_TRACE);
			assert (0);
		}
	}
	@property
	override
	string title() { mixin(S_TRACE);
		if (auto a = cast(Area)_eto) {
			return .tryFormat(_prop.msgs.viewNameEventTab, .objName!Area(_prop), a.id, a.name);
		} else if (auto a = cast(Battle)_eto) {
			return .tryFormat(_prop.msgs.viewNameEventTab, .objName!Battle(_prop), a.id, a.name);
		} else if (auto a = cast(Package)_eto) { mixin(S_TRACE);
			return .tryFormat(_prop.msgs.viewNameEventTab, .objName!Package(_prop), a.id, a.name);
		} else if (auto a = cast(SkillCard)_eto) { mixin(S_TRACE);
			return .tryFormat(_prop.msgs.viewNameEventTab, .objName!SkillCard(_prop), a.id, a.name);
		} else if (auto a = cast(ItemCard)_eto) { mixin(S_TRACE);
			return .tryFormat(_prop.msgs.viewNameEventTab, .objName!ItemCard(_prop), a.id, a.name);
		} else if (auto a = cast(BeastCard)_eto) { mixin(S_TRACE);
			return .tryFormat(_prop.msgs.viewNameEventTab, .objName!BeastCard(_prop), a.id, a.name);
		} else assert (0);
	}
	@property
	override
	void delegate(string) statusText() { return null; }
	private void refreshTitle() { mixin(S_TRACE);
		_comm.setTitle(_win, title);
		_eview.refreshTitle();
	}
	/// Returns: 編集中のイベントツリー所持者。
	@property
	EventTreeOwner eventTreeOwner() { mixin(S_TRACE);
		return _eto;
	}
	@property
	typeof(_eview) eventView() { mixin(S_TRACE);
		return _eview;
	}
	@property
	EventTreeView eventTreeView() { mixin(S_TRACE);
		return _eview.eventTreeView;
	}

	void movingShell() { mixin(S_TRACE);
		_eview.movingShell();
	}
	void moveShell() { mixin(S_TRACE);
		_eview.moveShell();
	}

	override {
		void cut(SelectionEvent se) { mixin(S_TRACE);
			_eview.cut(se);
		}
		void copy(SelectionEvent se) { mixin(S_TRACE);
			_eview.copy(se);
		}
		void paste(SelectionEvent se) { mixin(S_TRACE);
			_eview.paste(se);
		}
		void del(SelectionEvent se) { mixin(S_TRACE);
			_eview.del(se);
		}
		void clone(SelectionEvent se) { mixin(S_TRACE);
			_eview.clone(se);
		}
		@property
		bool canDoTCPD() { mixin(S_TRACE);
			return _eview.canDoTCPD;
		}
		@property
		bool canDoT() { mixin(S_TRACE);
			return _eview.canDoT;
		}
		@property
		bool canDoC() { mixin(S_TRACE);
			return _eview.canDoC;
		}
		@property
		bool canDoP() { mixin(S_TRACE);
			return _eview.canDoP;
		}
		@property
		bool canDoD() { mixin(S_TRACE);
			return _eview.canDoD;
		}
		@property
		bool canDoClone() { mixin(S_TRACE);
			return _eview.canDoClone;
		}
	}
	override
	bool openCWXPath(string path, bool shellActivate) { mixin(S_TRACE);
		return _eview.openCWXPath(path, shellActivate);
	}
	@property
	override
	string[] openedCWXPath() { mixin(S_TRACE);
		return _eview.openedCWXPath;
	}
}
